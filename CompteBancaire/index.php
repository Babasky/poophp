<?php

require_once 'classes/Compte.php';
require_once 'classes/CompteCourant.php';
require_once 'classes/CompteEpargne.php';

$compte1 = new CompteCourant("Baba",700,100);
$compte1->retirer(200);

$compte1->voirSolde();
$compte1->setTitulaire('jolie');
$compte1->getTitulaire();
echo '<br>';
$epargne = new CompteEpargne('Ali',700,5);
$epargne->verserInteret();
$epargne->voirSolde();
