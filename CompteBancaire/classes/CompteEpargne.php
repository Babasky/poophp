<?php

class CompteEpargne extends Compte
{
    private $interet;

    /**
     * Constructeur du compte epargne
     * @param string $titulaire
     * @param int $solde
     * @param int $interet
     */
    public function __construct(string $titulaire, int $solde,float $interet)
    {
        parent::__construct($titulaire,$solde);
        $this->interet = $interet;
    }


    /**
     * Get the value of interet
     */ 
    public function getInteret()
    {
        return $this->interet;
    }

    /**
     * Set the value of interet
     *
     * @return  self
     */ 
    public function setInteret($interet)
    {
        if ($interet>=0){
            $this->interet = $interet;
        }

        return $this;
    }

    public function verserInteret()
    {
        $this->solde = $this->solde + ($this->solde*$this->interet/100);
    }
}