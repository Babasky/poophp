<?php


abstract class Compte
{
    /**
     * @var string
     */
    protected $titulaire;
    /**
     * @var int
     */
    protected $solde;


    const TAUX_INTERET = 5;

    /**
     * Constructeur de nos comptes bancaire
     * @param string titulaire le nom du propiétaire
     * @param int solde le montant contenu dans le compte
     */
    public function __construct(string $titulaire, int $solde = 5000)
    {
        $this->titulaire = $titulaire;
        $this->solde = $solde;
    
    }

    /**
     * Getter du titulaire, il permet de recuperer le nom du titulaire
     */
    public function getTitulaire()
    {
        return $this->titulaire;
    }
    /**
     * Setter de titulaire, il permet de modifier le nom du titulaire
     */
    public function setTitulaire($titulaire)
    {
        if ($titulaire != "") {
            $this->titulaire = $titulaire;
        }
        return $this;
    }

    /**
     * Getter de solde, il permet de recuperer le solde du compte
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Setter de solde, il permet de modifier le solde du compte
     */
    public function setSolde($montant)
    {
        if ($montant>=0) {
            $this->solde = $montant;
        }
    }

    /**
     * fonction pour deposer de l'argent
     */
    public function deposer(int $montant)
    {
        if ($montant>0) {
            $this->solde +=$montant;
        }
        return $this;
    }

    /**
     * fonction pour consulter son solde
     */
    public function voirSolde()
    {
        echo "Le solde du compte est de ".$this->solde. " FCFA";
    }

    /**
     * fonction pour retirer de l'argent
     */
    public function retirer(int $montant)
    {
        if ($montant>0 && $this->solde>=$montant) {
            $this->solde -= $montant;
        }else{
            echo "Solde Insuffisant";
        }
         
    }

    public function __toString()
    {
        return "Vous consultez le compte de $this->titulaire, le solde est de $this->solde FCFA";
    }

    
    
}