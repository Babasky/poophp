<?php

class CompteCourant extends Compte
{
    private $decouvert;
    /**
     * constructeur du compte courant
     * @param string $titulaire
     * @param int $solde
     * @param int $decouvert
     */
    public function __construct(string $titulaire, int $solde, int $decouvert)
    {
        parent::__construct($titulaire, $solde);
        $this->decouvert = $decouvert;

    }

    public function getDecouvert()
    {
        return $this->decouvert;
    }

    public function setDecouvert($decouvert)
    {
        if ($decouvert>=0) {
            $this->decouvert = $decouvert;
        }
        return $this;
    }


    public function retirer(int $montant)
    {
        if ($montant > 0 && $this->solde - $montant >= -$this->decouvert) {
            $this->solde -= $montant;
        }else {
            echo "Solde insuffisant";
        }
    }
}