<?php


class TableauAlea{
    private $valeurs;

    public function __construct()
    {
        $this->valeurs = array();
    }

    public function ajouterNombre($limit)
    {
        for ($i=0; $i < $limit; $i++){ 
            $this->valeurs[] = random_int(1,100);
        }
    }

    public function nombreInf($nbr)
    {
        $tableau = array();
        foreach ($this->valeurs as $value) {
            if ($value < $nbr) {
                $tableau[] = $value;
            }
        }
        return $tableau;
    }


    public function nombreSup($nbr)
    {
        $tableau = array();
        foreach ($this->valeurs as $value) {
            if ($value >= $nbr) {
                $tableau[]= $value;
            }
        }

        return $tableau;
    }
}

$array = new TableauAlea();

$array->ajouterNombre(20);

$tableau1 = $array->nombreInf(50);

$tableau2 = $array->nombreSup(50);

echo " Les nombres inférieur à 50 sont: " .implode(', ', $tableau1);
echo "<br> Les nombres superieurs à 50 sont: " .implode(', ', $tableau2);