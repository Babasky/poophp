<?php

class Electeur extends Personne{
    private $bureau_vote;
    private $vote;

    public function __construct($nom,$prenom,$bureau_vote, $vote)
    {
        parent::__construct($nom,$prenom);
        $this->bureau_vote = $bureau_vote;
        $this->vote = $vote;
    }

    public function setBureau($bureau_vote)
    {
        $this->bureau_vote = $bureau_vote;
    }

    public function getBureau()
    {
        return $this->bureau_vote;
    }

    public function setVote($vote)
    {
        $this->vote = $vote;
    }

    public function getVote()
    {
        return $this->vote;
    }

    public function Voter(){
            
        $this->vote = true;
    
    }
    

    public function IsVoter(){
        if ($this->vote) {
            echo "L'electeur ". " ".$this->getPrenom(). " ".$this->getNom(). " ". " a voté dans le bureau". " ". $this->getBureau(); 
        }else {
            echo "La personne n'a pas voté";
        }
    }
}