<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Les exceptions et la gestion des erreurs</title>
</head>
<body>
    <?php
        function calculerProduit($a,$b){
            if (!is_numeric($a) || (!is_numeric($b))) {
                throw new Exception("Attention il faut que les deux paramètres soient des nombres");
                
            }else{
                return $a*$b;
            }
    }

    try{
        echo calculerProduit(4,'abs');
    }

    catch(Exception $e){
        echo "C'est une erreur :" .$e->getMessage();
        echo "<br> Code erreur :" .$e->getCode();
        echo "<br> URL erreur :" .$e->getFile();
        echo "<br> ligne erreur :" .$e->getLine();
    }



    ?>
</body>
</html>