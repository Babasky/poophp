<?php

class Utilisateur{
    private $nom;
    protected $prenom;
    protected $age = 25;
    protected $email;
    public $taille;

    const NREF_BASSE = 10;
    const NREF_HAUTE = 15;

    public function __construct($nom,$prenom,$email)
    {
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setEmail($email);
    }

    public function __call($method, $param)
    {
        echo "La methode ". $method. " n'existe pas ou n'est pas accessible";
        echo '<br>';
        echo "Arguments: " .implode(', ',$param);
    }

    public function listerAttributs()
    {
        foreach ($this as $attribut => $valeur) {
            echo $attribut . ' => ' .$valeur. ' <br>'; 
        }
    }


    public function __get($attribut)
    {
        echo "L'attribut ".$attribut. " auquel vous voulez acceder n'existe pas";   
    }

    public function __set($attribut, $valeur)
    {
        echo "L'attribut ".$attribut. " ne peut être modifié ou mis à jour avec la valeur " ;
    }

    public function __isset($attribut)
    {
        echo "L'attribut ".$attribut. " n'existe pas";
    }

    public function __unset($attribut)
    {
        echo "L'attribut ".$attribut. " que vous detruire n'existe pas ou est inaccessible";
    }

    public function __toString()
    {
        return "l'objet dont le nom ".$this->nom. " est un objet pas une chaine";
    }

    public function __invoke($arg)
    {
        echo "Vous êtes en train d'utiliser un objet comme une fonction et vous avez utilisé ".$arg. " comme argument";
    }

    public function SeConnecter()
    {
        echo 'je suis un utilisateur de votre application , je peux donc me connecter';
    }

    function SeDeconnecter()
    {
        echo 'je me deconnecte';
    }
    

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    public function setAge($age)
    {
        if ($age>120 || $age<0) {
            throw new Exception("l'age doit être entre 1 et 120");
        }else {
            
            $this->age = $age;
        }
    }

    public function setEmail($email)
    {
        if (filter_var($email,FILTER_VALIDATE_EMAIL)) {
            
            $this->email = $email;
        }
        else {
            throw new Exception("Attention, vous devez saisir une adresse mail valide");
            
        }
    }
    

    public function getNom()
    {
        return $this->nom;
    }


    public function getPrenom()
    {
        return $this->prenom;
    }

    public function getAge()
    {
        return $this->age;
    }


    public function getEmail()
    {
        return $this->email;
    }

    

    /*public function degreImplication()
    {
        if ($this->getNote()>=self::NREF_HAUTE) {
            echo 'Votre contribution est excellente';
        }

        elseif($this->getNote()>=self::NREF_BASSE){
            echo 'Votre contribution est moyenne';
        }

        else {
            echo 'Votre contribution est mauvaise';
        }
    }*/
}